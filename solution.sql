==================================
-- MYSQL - CULMINATING ACTIVITY --
==================================

-- 1. Return the customerName of the customers who are from the Philippines
SELECT customerName FROM customers WHERE country = "Philippines";

-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3. Return the product name and MSRP of the product named "The Titanic"
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

-- 4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- 5. Return the names of customers who have no registered stzte
SELECT customerName FROM customers WHERE state IS NULL;

-- 6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

-- 7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 300
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 300;

-- 8. Return the customer numbers of orders whose comments contain the string "DHL"
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

-- 9. Return the product lines whose text description mentions the phrase 'state of the art'
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

-- 10. Return the countries of customers without duplication
SELECT country FROM customers;

-- 11. Return the statuses of orders without duplication
SELECT DISTINCT status FROM orders;

-- 12. Return the customer names and countries of customers whose country is USA, France, or Canada
SELECT customerName, country FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";

-- 13. Return the first name, last name, and office's city of employees whose offices are in Tokyo
SELECT firstName, lastName, city FROM employees
JOIN offices ON employees.officeCode = offices.officeCode WHERE city = "Tokyo";

-- 14. Return the customer names of customers who were served by the employee named "Leslie Thompson"
SELECT customerName FROM customers
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber WHERE firstName = "Leslie" AND lastName = "Thompson";

-- 15. Return the product names and customer of products ordered by "Baane Mini Imports"
SELECT productName, customerName FROM customers
JOIN orders ON customers.customerNumber = orders.customerNumber
JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
JOIN products ON orderdetails.productCode = products.productCode WHERE customerName = "Baane Mini Imports";

-- 16. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country
SELECT firstName, lastName, customerName, offices.country FROM offices
JOIN employees ON offices.officeCode = employees.officeCode
JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber WHERE offices.country = customers.country;

-- 17. Return the product name and quantity in stock products that belong to the product line "planes" with stock quantities less than 1000
SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock < 1000;

-- 18. Return the customer's name with a phone number containing "+81"
SELECT customerName, phone FROM customers WHERE phone LIKE "%+81%";

==================
-- STRETCH GOAL --
==================

-- 1. Return the product name of the orders where customer name is "Baane Mini Imports"
SELECT productName FROM customers
JOIN orders ON customers.customerNumber = orders.customerNumber
JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
JOIN products ON orderdetails.productCode = products.productCode WHERE customerName = "Baane Mini Imports";

-- 2. Return the last name and first name of employees that reports to Anthony Bow
SELECT lastName, firstName FROM employees WHERE reportsTo = 1143;

-- 3. Return the product name of the product with the maximum MSRP
SELECT productName FROM products WHERE MSRP = (SELECT MAX(MSRP) FROM products);

-- 4. Return the number of products group by productline
SELECT productLine, COUNT(*) FROM products GROUP BY productLine;

-- 5. Return the number of products where status is cancelled
SELECT COUNT(orderNumber) FROM orders WHERE status = "cancelled";
